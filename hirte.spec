%global     with_python 0
Name:       hirte
Version:    0.4.0
Release:    2%{?dist}
Summary:    A systemd service controller for multi-nodes environments
License:    GPL-2.0-or-later
URL:        https://github.com/containers/hirte
Source0:    %{url}/archive/v%{version}/%{name}-%{version}.tar.gz
# Required to apply the patch
BuildRequires:  git

BuildRequires:  gcc
BuildRequires:  meson
BuildRequires:  systemd-devel
BuildRequires:  systemd-rpm-macros
BuildRequires:  golang-github-cpuguy83-md2man

Requires:   systemd
Recommends: hirte-selinux

%description
Hirte is a systemd service controller for multi-nodes environements with a
predefined number of nodes and with a focus on highly regulated environment
such as those requiring functional safety (for example in cars).

This package contains the controller and command line tool.

%post
%systemd_post hirte.service

%preun
%systemd_preun hirte.service

%postun
%systemd_postun_with_restart hirte.service

%files
%ghost %{_sysconfdir}/hirte/hirte.conf
%doc README.md
%doc README.developer.md
%license LICENSE
%{_bindir}/hirte
%{_datadir}/dbus-1/interfaces/org.containers.hirte.Job.xml
%{_datadir}/dbus-1/interfaces/org.containers.hirte.Manager.xml
%{_datadir}/dbus-1/interfaces/org.containers.hirte.Monitor.xml
%{_datadir}/dbus-1/interfaces/org.containers.hirte.Node.xml
%{_datadir}/dbus-1/system.d/org.containers.hirte.conf
%{_datadir}/hirte/config/hirte.conf
%{_mandir}/man1/hirte.*
%{_mandir}/man5/hirte.conf.*
%{_sysconfdir}/hirte/hirte.conf.d/README.md
%{_unitdir}/hirte.service
%{_unitdir}/hirte.socket

#--------------------------------------------------

%package agent
Summary:    Hirte service controller agent
Requires:   systemd

%description agent
Hirte is a systemd service controller for multi-nodes environements with a
predefined number of nodes and with a focus on highly regulated environment
such as those requiring functional safety (for example in cars).

This package contains the node agent.

%post agent
%systemd_post hirte-agent.service

%preun agent
%systemd_preun hirte-agent.service

%postun agent
%systemd_postun_with_restart hirte-agent.service

%files agent
%ghost %{_sysconfdir}/hirte/agent.conf
%doc README.md
%license LICENSE
%{_bindir}/hirte-agent
%{_bindir}/hirte-proxy
%{_datadir}/dbus-1/system.d/org.containers.hirte.Agent.conf
%{_datadir}/hirte-agent/config/agent.conf
%{_datadir}/dbus-1/interfaces/org.containers.hirte.Agent.xml
%{_mandir}/man1/hirte-agent.*
%{_mandir}/man1/hirte-proxy.*
%{_mandir}/man5/hirte-agent.conf.*
%{_sysconfdir}/hirte/agent.conf.d/README.md
%{_unitdir}/hirte-agent.service
%{_userunitdir}/hirte-agent.service
%{_unitdir}/hirte-proxy@.service
%{_userunitdir}/hirte-proxy@.service
%{_unitdir}/hirte-dep@.service
%{_userunitdir}/hirte-dep@.service

#--------------------------------------------------

%package selinux
Summary:  Hirte SELinux policy
BuildRequires: checkpolicy
BuildRequires: selinux-policy-devel

%if "%{_selinux_policy_version}" != ""
Requires: selinux-policy >= %{_selinux_policy_version}
%endif

Requires(post): policycoreutils
Requires(post): policycoreutils-python-utils
Requires(postun): policycoreutils-python-utils

%global selinuxtype	targeted

%description selinux
SELinux policy associated with the hirte and hirte-agent daemons

%files selinux
%{_datadir}/selinux/devel/include/services/hirte.if
%{_datadir}/selinux/packages/hirte.pp.bz2
%{_mandir}/man8/hirte*selinux.*

%post selinux
%selinux_modules_install -s %{selinuxtype} %{_datadir}/selinux/packages/hirte.pp.bz2
restorecon -R %{_bindir}/hirte* &> /dev/null || :
semanage port -a -t hirte_port_t -p udp 842 || true
semanage port -a -t hirte_port_t -p tcp 842 || true

%postun selinux
if [ $1 -eq 0 ]; then
   %selinux_modules_uninstall -s %{selinuxtype} hirte
   semanage port -d -p udp 842 || true
   semanage port -d -p tcp 842 || true
   restorecon -R %{_bindir}/hirte* &> /dev/null || :
fi

#--------------------------------------------------

%package ctl
Summary:  Hirte service controller command line tool
Requires: %{name} = %{version}-%{release}
Provides: hirtectl = %{version}-%{version}

%description ctl
Hirte is a systemd service controller for multi-nodes environements with a
predefined number of nodes and with a focus on highly regulated environment
such as those requiring functional safety (for example in cars).
This package contains the service controller command line tool.

%files ctl
%doc README.md
%license LICENSE
%{_bindir}/hirtectl
%{_mandir}/man1/hirtectl.*

#--------------------------------------------------

%if %{with_python}

%package -n python3-pyhirte
Summary: Python bindings for Hirte
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
Requires:       python3-dasbus

%description -n python3-pyhirte
pyhirte is a python module to access the public D-Bus API of hirte.
It contains typed python code that is auto-generated from hirte's
API description and manually written code to simplify recurring tasks.

%files -n python3-pyhirte
%license LICENSE
%doc README.md
%{python3_sitelib}/pyhirte-*.egg-info/
%{python3_sitelib}/pyhirte/

%endif

#--------------------------------------------------

%prep
%autosetup -S git

%build
%meson -Dapi_bus=system
%meson_build

%if %{with_python}
pushd src/bindings/python
%py3_build
popd
%endif

%install
%meson_install

%if %{with_python}
pushd src/bindings/python
%py3_install
popd
%endif

%check
%meson_test


%changelog
* Mon Jul 17 2023 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.4.0-2
- Fix the conditional used to enable/disable the python3-pyhirte subpackage

* Mon Jul 17 2023 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.4.0-1
- Update to 0.4.0
- Introduce the python3-pyhirte subpackage

* Fri Jun 09 2023 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.3.0-1
- Update to 0.3.0
- Backport patch from PR #355 which fixes building on i686

* Tue May 02 2023 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.2.0-1
- Update to 0.2.0
- Introduce the hirte-selinux sub-package

* Tue Mar 28 2023 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.1.1-3
- Drop the man page for hirtectl from the main hirte package

* Mon Mar 27 2023 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.1.1-2
- Remove hirtectl from the hirte package since it is now in its own subpackage

* Mon Mar 27 2023 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.1.1-1
- Update to 0.1.1
- Adjust Source0 to point to /archive/v<version>/hirte-<version>.tar.gz
- Adjust the location of dbus-1/system.d/org.containers.hirte.conf and
  bus-1/system.d/org.containers.hirte.Agent.conf so they are in _datadir
- Add the hirte-ctl subpackage (which provides hirtectl for convenience)

* Wed Mar 22 2023 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.1.0-5
- Adjust summary and description according to the changes made upstream

* Wed Mar 22 2023 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.1.0-4
- Fix the Source0 to point to a resolvable url
- Replace tabs with spaces

* Tue Mar 21 2023 Martin Perina <mperina@redhat.com> - 0.1.0-3
- Move the different files section near the different package definition

* Tue Mar 21 2023 Martin Perina <mperina@redhat.com> - 0.1.0-2
- Make rpmlint happier

* Tue Mar 21 2023 Martin Perina <mperina@redhat.com> - 0.1.0-1
- Initial release

